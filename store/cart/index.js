export const namespaced = () => { true }

export const state = () => ({
    counter: 4,
    keranjangUser: []
})

export const mutations = {
    increment(state) {
        state.counter++
    },
    getCount(state) {
        // mutate state
        state.count
    },

    setKeranjang(state, data) {
        // mutate state
        state.keranjangUser = data
    }
}

export const actions = {
    addkeranjangUser({ commit }, data) {
        commit('setKeranjang', data)
    },
    getCount({ commit }) {
        commit('getCount')
    },
    getkeranjangUser({ commit }) {
        state => state.keranjangUser
    }
}

export const getters = {
    keranjangUsers: state => state.keranjangUser,
    Count: state => state.count,
    cartTotal: state => {
        // const data = Array.isArray(state.keranjangUser) ?
        //     state.keranjangUser.reduce((acc, keranjangUsers) => {
        //         return (keranjangUsers.qty * keranjangUsers.price) + acc;
        //     }, 0).toFixed(2) : 0;
        // return data;
        if (state.keranjangUser.length > 0) {

            return state.keranjangUser.reduce((acc, keranjangUsers) => {
                return (keranjangUsers.qty * keranjangUsers.price) + acc;
            }, 0).toFixed(2)
        } else {
            return 0;
        }

    },
    cartQuantity: state => {
        return state.keranjangUser.reduce((acc, keranjangUser) => {
            return keranjangUser.qty + acc;
        }, 0);
    }
}