import axios from 'axios';

export const namespaced = () => { true }

export const state = () => ({
    counter: 4,
    productItems: []
})

export const mutations = {
    UPDATE_PRODUCT_ITEMS(state, payload) {
        state.productItems = payload;
    }
}

export const actions = {
    getProductItems({ commit }) {
        axios.get(`http://localhost:8000/api/products`).then((response) => {
            commit('UPDATE_PRODUCT_ITEMS', response.data.data.data)
        });
    }
}

export const getters = {
    productItems: state => state.productItems,
    productItemById: (state) => (id) => {
        return state.productItems.find(productItem => productItem.id === id)
    }
}